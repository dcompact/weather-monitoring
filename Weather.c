/* Program      :Weather Monitoring System
 * File name    :Weather.c
 * Programmer   : Ankit Guhe, Shivani Joshi, Manjot Hansra
 * Description  :Function to monitor live weather conditions and displaying them on serial port 
                 through ADC calibrations with use of Temp. Sensor,Humidity Sensor and LDR sensor. 
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "stm32f3xx_hal.h"
#include "stm32f3_discovery.h"
#include "common.h" 

/* Declartions of all functions */
void initialization();
void delay(int x);
void tempSensor();
void humiditySensor();
void lightSensor();

// FUNCTION    : WeatherSystem()
// DESCRIPTION : Continously monitoring all sensors and 
//               displaying weather conditons on Serial Monitor. 
// PARAMETERS  : int
// RETURN      : Void 
void WeatherSystem(int mode)
{
	if(mode != CMD_INTERACTIVE)
    {
     return;
	}  
initialization();
while(1)
{
HAL_Delay(2000);                                                            // Giving Delay 
tempSensor();                                                               // Calling Temperature sensor function
humiditySensor();                                                           // Calling humidity sensor function
lightSensor();                                                              // Calling Light sensor function
}
}
ADD_CMD("check",WeatherSystem,"        monitoring weather conditions")      // Assigning MINiCOM command to chech Weather conditions 

 // FUNCTION    : initialization()
 // DESCRIPTION : initializing GPIO PINs as analog 
 //               and configuring ADC channels 
 // PARAMETERS  : Void
 // RETURN      : Void 
void initialization()                                                       
{     
RCC->CFGR2 |= RCC_CFGR2_ADCPRE12_DIV2;                                      // Configure the ADC clock 
RCC->AHBENR |= RCC_AHBENR_ADC12EN;                                          // Enable ADC1, ADC2 clock
RCC->CFGR2 |= RCC_CFGR2_ADCPRE34_DIV2;                                      // Configure the ADC clock
RCC->AHBENR |= RCC_AHBENR_ADC34EN;                                          // Enable ADC3, ADC4 clock 
RCC->AHBENR |= RCC_AHBENR_GPIOAEN;                                          // GPIOA Periph clock enable
RCC->AHBENR |= RCC_AHBENR_GPIOBEN;                                          // GPIOB Periph clock enable
RCC->AHBENR |= RCC_AHBENR_GPIOEEN;                                          // GPIOE Periph clock enable

GPIOA->MODER |= 0x00000003;        	                                        // Configure PA0 as analog input
GPIOB->MODER |= 0x0C000000;                                            	    // Configure PB13 as analog input
GPIOE->MODER |= 0xc0000000;                                                 // Configure PE15 as analog input
} 

 // FUNCTION    : humiditySensor()
 // DESCRIPTION : Calibrating the value of humidity sensor 
 //               that is connected at PA0 using ADC1
 // PARAMETERS  : Void
 // RETURN      : Void 
void humiditySensor()
{

unsigned int calibration_value;                                            // Declaring calibration_value variable as unsigned int
unsigned int ADC1ConvertedValue;                                           // Declaring ADC1ConvertedValue variable as unsigned int
ADC1->CR &= ~ADC_CR_ADVREGEN; 
ADC1->CR |= ADC_CR_ADVREGEN_0;    	                                       // 01: ADC Voltage regulator enabled
delay(10);                                                                 // Insert delay equal to 10 μs 
ADC1->CR &= ~ADC_CR_ADCALDIF;     	                                       // calibration in Single-ended inputs Mode.
ADC1->CR |= ADC_CR_ADCAL;            	                                   // Start ADC calibration 
while (ADC1->CR & ADC_CR_ADCAL);                                           // wait until calibration done
calibration_value = ADC1->CALFACT; 	                                       // Get Calibration Value ADC1 
printf("Humidity calibration value is %u\n",calibration_value);            // Print value of Humidity sensor
ADC1->CFGR &= ~ADC_CFGR_CONT;      	                                       //ADC_ContinuousConvMode_Enable
ADC1->CFGR &= ~ADC_CFGR_RES;     	                                       // 12-bit data resolution
ADC1->CFGR &= ~ADC_CFGR_ALIGN;                          	               // Right data alignment 
/* ADC1 regular channel1 configuration */
ADC1->SQR1 |=  ADC_SQR1_SQ1_0; 		                                       // SQ1 = 0x01, start converting ch1 
ADC1->SQR1 &= ~ADC_SQR1_L; 		                                           // ADC regular channel sequence length = 0 => 1 conversion/sequence
ADC1->SMPR1 |= ADC_SMPR1_SMP7_1 | ADC_SMPR1_SMP7_0;                        // = 0x03 => sampling time 7.5 ADC clock cycles
ADC1->CR |= ADC_CR_ADEN;             	                                   // Enable ADC1 
while(!ADC1->ISR & ADC_ISR_ADRD);    	                                   // wait for ADRDY 
ADC1->CR |= ADC_CR_ADSTART;          	                                   // Start ADC1 Software Conversion
while(!(ADC1->ISR & ADC_ISR_EOC));  	                                   // Test EOC flag
ADC1ConvertedValue = ADC1->DR;      	                                   // Get ADC1 converted data
ADC1ConvertedValue=(ADC1ConvertedValue*100)/4095;                          // calibrating value of moisture sensor 
printf("Mositure sensor  %u\n", ADC1ConvertedValue);                       // Printing moisture sensor value
if(ADC1ConvertedValue>=70 && ADC1ConvertedValue<=100)                      // Low moisture range conditions
{
BSP_LED_On(6);                                                             // LED indication for low Moisture  
printf("Its Raining......\n");                                             // Printing Rain inidiactions warning
printf("\n");
}
else if(ADC1ConvertedValue>=0 && ADC1ConvertedValue<=69)                   // HIGH moisture range conditions
{ 
BSP_LED_Off(6);                                                            // LED indication for high Moisture  
}
}

 // FUNCTION    : tempSensor()
 // DESCRIPTION : Calibrating the value of temperature sensor  
 //               that is connected at PE15 using ADC4
 // PARAMETERS  : Void
 // RETURN      : Void  
void tempSensor()                                                          // Initializing Temperature sensor Function
{
 unsigned int ADC4ConvertedValue;                                           // Declaring ADC4ConvertedValue variable as unsigned int
 unsigned int calibration_value4;                                           // Declaring calibration_value4 variable as unsigned int
/* Calibration procedure */
 ADC4->CR &= ~ADC_CR_ADVREGEN;
 ADC4->CR |= ADC_CR_ADVREGEN_0;                                             // 04: ADC Voltage regulator enabled
 delay(10); 			                                                    // Insert delay equal to 10 μs
 ADC4->CR &= ~ADC_CR_ADCALDIF;                                              // calibration in Single-ended inputs Mode.
 ADC4->CR |= ADC_CR_ADCAL;                                                  // Start ADC calibration
 while (ADC4->CR & ADC_CR_ADCAL);                                           // wait until calibration done
 calibration_value4 = ADC4->CALFACT;                                        // Get Calibration Value ADC1
 printf("Temperature calibration value is %u\n",calibration_value4);
 /* ADC configuration */
 ADC4->CFGR &= ~(ADC_CFGR_CONT);                                            //ADC_ContinuousConvMode_Enable
 ADC4->CFGR &= ~ADC_CFGR_RES;                                               // 12-bit data resolution
 ADC4->CFGR &= ~ADC_CFGR_ALIGN;                                             // Right data alignment*/
 /* ADC1 regular channel7 configuration */ 
 ADC4->SQR1 |=  ADC_SQR1_SQ1_1;                                             // SQ1 = 0x02, start converting ch2
 ADC4->SQR1 &= ~ADC_SQR1_L;                                                 // ADC regular channel sequence length = 0 => 1 conversion/sequence
 ADC4->SMPR1 |= ADC_SMPR1_SMP1_1 | ADC_SMPR1_SMP1_0;                        // = 0x03 => sampling time 7.5 ADC clock cycles
 ADC4->CR |= ADC_CR_ADEN;                                                   // Enable ADC4
 while(!ADC4->ISR & ADC_ISR_ADRD);                                          // wait for ADRDY
 ADC4->CR |= ADC_CR_ADSTART;                                                // Start ADC4 Software Conversion
 while(!(ADC4->ISR & ADC_ISR_EOC));                                         // Test EOC flag
 ADC4ConvertedValue = ADC4->DR;                                             // Get ADC4 converted data
 printf("tempertature calibrated value is   = %u \n", ADC4ConvertedValue);
 ADC4ConvertedValue= (0.081*ADC4ConvertedValue)-50;                         // calibration for Temperature sensor 
 printf("tempertature sensor  = %uC\n", ADC4ConvertedValue);                // Printing Temperture sensor value
 printf("\n");
 if(ADC4ConvertedValue>=0 && ADC4ConvertedValue<=15)                        // Low temperature range conditions
 { 
BSP_LED_On(4);                                                              // LED indication for low temperature  
printf("Temperature is LOW!!!\n");                                          // Printing LOW Temperture inidiactions 
printf("\n");
}
else if(ADC4ConvertedValue>=16 && ADC4ConvertedValue<=25)                   // Cloudy weather range conditions
{ 
BSP_LED_On(3);                                                              // LED indication for Cloudy weather  
printf("Its Cloudy!!!\n");                                                  // Printing Cloudy weather for alert. 
}
else if(ADC4ConvertedValue>=26 && ADC4ConvertedValue<=100)                  // High temperature range conditions
{ 
BSP_LED_Off(4);                                                             // LED indication for low temperature  
printf("Temperature is HIGH!!!\n");                                         // Printing HIGH Temperture for warning 
}
}

 // FUNCTION    : lightSensor()
 // DESCRIPTION : Calibrating the value of light sensor that 
 //               is connected at PB13 using ADC3
 // PARAMETERS  : Void
 // RETURN      : Void  
void lightSensor()                                                          // Function to get Light sensor input
{
unsigned int ADC3ConvertedValue;                                            // Declaring ADC3ConvertedValue variable as unsigned int
unsigned int calibration_value3;                                            // Declaring calibration_value3 variable as unsigned int
 /* Calibration procedure */
 ADC3->CR &= ~ADC_CR_ADVREGEN;
 ADC3->CR |= ADC_CR_ADVREGEN_0;                                             // 01: ADC Voltage regulator enabled
 delay(10); 			                                                    // Insert delay equal to 10 μs
 ADC3->CR &= ~ADC_CR_ADCALDIF;                                              // calibration in Single-ended inputs Mode.
 ADC3->CR |= ADC_CR_ADCAL;                                                  // Start ADC calibration
 while (ADC3->CR & ADC_CR_ADCAL);                                           // wait until calibration done
 calibration_value3 = ADC3->CALFACT;                                        // Get Calibration Value ADC1
 printf("Light sensor calibration value is %u\n",calibration_value3);
 /* ADC configuration */
 ADC3->CFGR &= ~(ADC_CFGR_CONT);                                            //ADC_ContinuousConvMode_Enable
 ADC3->CFGR &= ~ADC_CFGR_RES;                                               // 12-bit data resolution
 ADC3->CFGR &= ~ADC_CFGR_ALIGN;                                             // Right data alignment*/
 /* ADC1 regular channel7 configuration */
 ADC3->SQR1 |=  ADC_SQR1_SQ1_2|ADC_SQR1_SQ1_0;                              // SQ1 = 0x05, start converting ch5
 ADC3->SQR1 &= ~ADC_SQR1_L;                                                 // ADC3 regular channel sequence length = 0 => 1 conversion/sequence
 ADC3->SMPR1 |= ADC_SMPR1_SMP1_1 | ADC_SMPR1_SMP1_0;                        // = 0x03 => sampling time 7.5 ADC clock cycles
 ADC3->CR |= ADC_CR_ADEN;                                                   // Enable ADC3
 while(!ADC3->ISR & ADC_ISR_ADRD);                                          // wait for ADRDY
 ADC3->CR |= ADC_CR_ADSTART;                                                // Start ADC3 Software Conversion
 while(!(ADC3->ISR & ADC_ISR_EOC));                                         // Test EOC flag
 ADC3ConvertedValue = ADC3->DR;                                             // Get ADC3 converted data
 ADC3ConvertedValue=(ADC3ConvertedValue*100)/4095;                          //ADC3ConvertedValue-=500;
 printf("Light sensor  = %u \n", ADC3ConvertedValue);                       // Print value of light sensor
 printf("\n");
if(ADC3ConvertedValue>=30 && ADC3ConvertedValue<=100)                       // Range for Light intensity 
{
BSP_LED_On(5);                                                              // LED indication for suny day
printf("Its sunny.... \n");                                                 // Displaying sunny day 
printf("\n");
}
else if(ADC3ConvertedValue>=0 && ADC3ConvertedValue<=29)                    // Range for low light intensity 
{
BSP_LED_Off(5);                                                             // LED indication for Night
printf("Night mode !!!\n");                                                 // Displaying Night mode for indication.
printf("\n");
}
}

 // FUNCTION    : delay()
 // DESCRIPTION : Generating Delay with the use of for loop 
 // PARAMETERS  : int
 // RETURN      : Void 
void delay(int x)    			                                           
{
	for(int i=0;i<x;i++);
}



